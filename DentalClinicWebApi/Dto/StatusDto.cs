﻿using DentalClinicWebApi.Models.Base;

namespace DentalClinicWebApi.Dto
{
    public class StatusDto : BaseModel
    {
        public string Name { get; set; }
        public string Price { get; set; }
    }
}
