﻿using DentalClinicWebApi.Models.Base;

namespace DentalClinicWebApi.Dto
{
    public class DatesDto : BaseModel
    {
        public int AccountId { get; set; }
        public int StatusId { get; set; }
        public bool IsDone { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
    }
}
