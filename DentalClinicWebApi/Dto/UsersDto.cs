﻿using DentalClinicWebApi.Models.Base;

namespace DentalClinicWebApi.Dto
{
    public class UsersDto : BaseModel
    {
        public int AccountsId { get; set; }
        public AccountsDto Account { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? UserType { get; set; }
    }
}
