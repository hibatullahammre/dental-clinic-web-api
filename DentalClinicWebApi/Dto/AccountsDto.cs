﻿using DentalClinicWebApi.Models;
using DentalClinicWebApi.Models.Base;
using DentalClinicWebApi.Models.Enums;

namespace DentalClinicWebApi.Dto
{
    public class AccountsDto : BaseModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool Gender { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        public AccountType accountsType { get; set; }
    }
}
