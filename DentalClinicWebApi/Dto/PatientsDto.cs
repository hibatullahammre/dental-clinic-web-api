﻿using DentalClinicWebApi.Models.Base;

namespace DentalClinicWebApi.Dto
{
    public class PatientsDto : BaseModel
    {
        public int AccountID { get; set; }
        public virtual AccountsDto Account { get; set; }
        public string Job { get; set; }
        public string status { get; set; }
        public string MainComplaint { get; set; }
        public string SunniStory { get; set; }
        public string DentistVisit { get; set; }
        public string TraitType { get; set; }
        public string DrugSensitivity { get; set; }
        public bool? Gravid { get; set; }
        public bool? Contraceptive { get; set; }
        public string Period { get; set; }
        public bool? Sugar { get; set; }
        public bool? BloodPressure { get; set; }
        public bool? Heart { get; set; }
        public string Notes { get; set; }
        public string MedicationsSensitivity { get; set; }
        public string traitPlan { get; set; }
        public string OtherDecease { get; set; }
        public bool? Smoking { get; set; }
        public bool? Alcohol { get; set; }
    }
}
