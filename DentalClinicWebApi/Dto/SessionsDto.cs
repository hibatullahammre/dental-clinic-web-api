﻿using DentalClinicWebApi.Models.Base;

namespace DentalClinicWebApi.Dto
{
    public class SessionsDto : BaseModel
    {
        public int? StatusID { get; set; }
        public int? AccountID { get; set; }
        public string Title { get; set; }
        public string Num { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public string Fund { get; set; }
    }
}
