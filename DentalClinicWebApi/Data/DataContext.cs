﻿using DentalClinicWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicWebApi.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Dates> Dates { get; set; }
        public DbSet<Funds> Funds { get; set; }
        public DbSet<Patients> Patients { get; set; }
        public DbSet<Sessions> Sessions { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
