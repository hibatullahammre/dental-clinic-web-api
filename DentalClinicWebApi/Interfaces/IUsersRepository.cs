﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IUsersRepository
    {
        ICollection<Users> GetUsers();
        Users GetUserById(int userId);
        bool UserExist(int userId);
        bool AddUser(Users user);
        bool UpdateUser(Users user);
        bool DeleteUser(Users user);
        bool Save();
    }
}
