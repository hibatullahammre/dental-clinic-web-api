﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface ISeetingsRepository
    {
        ICollection<Settings> GetSettings();
        Settings GetSettingsById(int settingId);
        Settings GetSettingsByName(string name);
        bool SettingExist(int settingId);
        bool UpdateSettings(Settings settings);
        bool Save();
    }
}
