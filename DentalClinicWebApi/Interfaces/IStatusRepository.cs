﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IStatusRepository
    {
        ICollection<Status> GetStatus();
        Status GetStatusById(int id);
        Status GetStatusByName(string name);
        bool StatusExist(int id);
        bool AddStatus(Status status);
        bool UpdateStatus(Status status);   
        bool DeleteStatus(Status status);
        bool Save();
    }
}
