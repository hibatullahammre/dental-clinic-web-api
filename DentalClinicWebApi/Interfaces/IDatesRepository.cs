﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IDatesRepository
    {
        ICollection<Dates> GetDates();
        Dates GetDate(int dateId);
        ICollection<Dates> GetDates(DateTime Date);
        ICollection<Dates> GetDatesByAccount(int accountId);
        bool DateExists(int dateId);
        bool CreateDate(Dates date);
        bool UpdateDate(Dates date);
        bool DeleteDate(Dates date);
        bool Save();
    }
}
