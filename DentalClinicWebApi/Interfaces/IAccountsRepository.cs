﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IAccountsRepository
    {
        ICollection<Accounts> GetAccounts();
        ICollection<Accounts> GetEmployees();
        ICollection<Accounts> GetPatients();
        ICollection<Accounts> GetUsers();
        Accounts GetAccountByID(int accountId);
        Accounts GetAccountByName(string name);    
        bool IsAccountExist(int id);
        bool CreateAccount(Accounts account);
        bool UpdateAccount(Accounts account);
        bool DeleteAccount(Accounts account);
        bool Save();
    }
}
