﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IPatientsRepository
    {
        ICollection<Patients> GetPatients();
        Patients GetPatientById(int patientId);
        bool PatientExist(int patientId);
        bool AddPatient(Patients patient);
        bool UpdatePatient(Patients patient);
        bool DeletePatient(Patients patient);
        bool Save();
    }
}
