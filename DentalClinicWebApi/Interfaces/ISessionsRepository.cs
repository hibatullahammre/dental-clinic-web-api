﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface ISessionsRepository
    {
        ICollection<Sessions> GetSessions();
        ICollection<Sessions> GetStatusSessionForpatient(int statusId, int accountId);
        ICollection<Sessions> GetPatientSessions(int accountId);
        ICollection<Sessions> GetDailySession(DateTime sessionDate);
        Sessions GetSessionById(int sessionId);
        bool SessionExist(int sessionId);
        bool AddSession(Sessions session);
        bool UpdateSession(Sessions session);
        bool DeleteSession(Sessions session);
        bool Save();
    }
}
