﻿using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Interfaces
{
    public interface IFundsRepository
    {
        ICollection<Funds> GetFunds();
        Funds GetFundsById(int fundsId);
        ICollection<Funds> GetDailyFunds(DateTime date);
        ICollection<Funds> GetAccountCheckUp(int accountId, DateTime date);
        ICollection<Funds> GetMonthlyFunds(DateTime start, DateTime end);
        bool FundsExist(int fundsId);
        bool AddFunds(Funds funds);
        bool UpdateFunds(Funds funds);
        bool DeleteFunds(Funds funds);
        bool Save();
    }
}
