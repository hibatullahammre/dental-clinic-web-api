﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Helper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Accounts, AccountsDto>();
            CreateMap<AccountsDto, Accounts>();
            CreateMap<Dates, DatesDto>();
            CreateMap<DatesDto, Dates>();
            CreateMap<Status, StatusDto>();
            CreateMap<StatusDto, Status>();
            CreateMap<SessionsDto, Sessions>();
            CreateMap<Sessions, SessionsDto>();
            CreateMap<Funds, FundsDto>();
            CreateMap<FundsDto, Funds>();
            CreateMap<PatientsDto, Patients>();
            CreateMap<Patients, PatientsDto>();
            CreateMap<Users, UsersDto>();
            CreateMap<UsersDto, Users>();
        }
    }
}
