﻿namespace DentalClinicWebApi.Models
{
    public class Sessions : Base.BaseModel
    {
        public int? StatusID { get; set; }
        public virtual Status Status { get; set; }
        public int? AccountID { get; set; }
        public virtual Accounts Account { get; set; }
        public string Title { get; set; }
        public string Num { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public string Fund { get; set; }
    }
}
