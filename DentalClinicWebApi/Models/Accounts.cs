﻿using DentalClinicWebApi.Models.Enums;

namespace DentalClinicWebApi.Models
{
    public class Accounts : Base.BaseModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool Gender { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        public AccountType accountsType { get; set; }

        public ICollection<Dates> Dates { get; set; }
        public ICollection<Funds> Funds { get; set; }
    }
}
