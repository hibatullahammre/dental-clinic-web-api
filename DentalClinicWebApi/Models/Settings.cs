﻿namespace DentalClinicWebApi.Models
{
    public class Settings : Base.BaseModel
    {
        public string Name { get; set; }
        public string Value { get; set; } 
    }
}
