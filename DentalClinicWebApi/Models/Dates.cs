﻿namespace DentalClinicWebApi.Models
{
    public class Dates : Base.BaseModel
    {
        public virtual Accounts Account { get; set; }
        public int AccountId { get; set; }
        public Status Status { get; set; }
        public int StatusId { get; set; }
        public bool IsDone { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
    }
}
