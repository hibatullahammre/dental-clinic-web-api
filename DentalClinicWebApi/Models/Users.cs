﻿namespace DentalClinicWebApi.Models
{
    public class Users : Base.BaseModel
    {
        public int AccountsId { get; set; }
        public Accounts Account { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? UserType { get; set; }
    }
}
