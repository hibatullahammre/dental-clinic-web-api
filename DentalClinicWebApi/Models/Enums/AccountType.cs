﻿namespace DentalClinicWebApi.Models.Enums
{
    public enum AccountType
    {
        Employee,
        Patient,
        User,
    }
}
