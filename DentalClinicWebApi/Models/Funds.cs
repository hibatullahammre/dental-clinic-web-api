﻿namespace DentalClinicWebApi.Models
{
    public class Funds : Base.BaseModel
    {
        public virtual Accounts Account { get; set; }
        public int AccountId { get; set; }
        public virtual Sessions Session { get; set; }
        public int? SessionId { get; set; }
        public string Name { get; set; }
        public double? InComing { get; set; }
        public double? OutComing { get; set; }
        public bool? IsFixed { get; set; }
        public DateTime? Date { get; set; }
        public string Notes { get; set; }
        public string Number { get; set; }
    }
}
