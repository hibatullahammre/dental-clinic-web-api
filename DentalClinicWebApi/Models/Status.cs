﻿namespace DentalClinicWebApi.Models
{
    public class Status : Base.BaseModel
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public ICollection<Dates> Dates { get; set; }
    }
}
