﻿using System.ComponentModel.DataAnnotations;

namespace DentalClinicWebApi.Models.Base
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? IsExist { get; set; }

        public BaseModel()
        {
            LastModified = DateTime.Now;
            IsExist = true;
        }
    }
}
