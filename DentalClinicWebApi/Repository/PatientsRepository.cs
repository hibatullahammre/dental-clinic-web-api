﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicWebApi.Repository
{
    public class PatientsRepository : IPatientsRepository
    {
        private readonly DataContext _context;

        public PatientsRepository(DataContext context)
        {
            _context = context;
        }

        public Patients GetPatientById(int patientId)
        {
            return _context.Patients.Include(c => c.Account)
                .Where(p => p.IsExist.Value && p.Account.accountsType == Models.Enums.AccountType.Patient && p.Id == patientId).FirstOrDefault();
        }

        public ICollection<Patients> GetPatients()
        {
            return _context.Patients.Include(p => p.Account)
                .Where(p => p.IsExist.Value && p.Account.accountsType == Models.Enums.AccountType.Patient).ToList();
        }

        public bool AddPatient(Patients patient)
        {
            _context.Add(patient);
            return Save();
        }

        public bool DeletePatient(Patients patient)
        {
            patient.IsExist = false;
            return Save();
        }

        public bool PatientExist(int patientId)
        {
            return _context.Patients.Any(p => p.IsExist.Value && p.Id == patientId);
        }

        public bool UpdatePatient(Patients patient)
        {
            _context.Update(patient);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
