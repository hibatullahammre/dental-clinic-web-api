﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicWebApi.Repository
{
    public class FundsRepository : IFundsRepository
    {
        private readonly DataContext _context;

        public FundsRepository(DataContext context)
        {
            _context = context;
        }

        public bool AddFunds(Funds funds)
        {
            _context.Add(funds);
            return Save();
        }

        public bool DeleteFunds(Funds funds)
        {
            funds.IsExist = false;
            return Save();
        }

        public bool FundsExist(int fundsId)
        {
            return _context.Funds.Any(f => f.IsExist.Value && f.Id == fundsId);
        }

        public ICollection<Funds> GetAccountCheckUp(int accountId, DateTime date)
        {
            return _context.Funds.Include(f => f.Account)
                .Where(f => f.AccountId == accountId && f.Date.Value.Date == date.Date).ToList();
        }

        public ICollection<Funds> GetDailyFunds(DateTime date)
        {
            return _context.Funds.Where(f => f.IsExist.Value && f.Date == date.Date).ToList();
        }

        public ICollection<Funds> GetFunds()
        {
            return _context.Funds.Where(f => f.IsExist.Value).ToList();
        }

        public Funds GetFundsById(int fundsId)
        {
            return _context.Funds.Where(f => f.IsExist.Value && f.Id == fundsId).FirstOrDefault();
        }

        public ICollection<Funds> GetMonthlyFunds(DateTime start, DateTime end)
        {
            return _context.Funds.Where(f => f.Date >= start.Date && f.Date <= end.Date && f.IsExist.Value).ToList();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }

        public bool UpdateFunds(Funds funds)
        {
            _context.Update(funds);
            return Save();
        }
    }
}
