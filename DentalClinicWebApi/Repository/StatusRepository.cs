﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Repository
{
    public class StatusRepository : IStatusRepository
    {
        private readonly DataContext _context;

        public StatusRepository(DataContext context)
        {
            _context = context;
        }

        public ICollection<Status> GetStatus()
        {
            return _context.Status.Where(s => s.IsExist == true).ToList();
        }

        public Status GetStatusById(int id)
        {
            return _context.Status.Where(s => s.Id == id && s.IsExist == true).FirstOrDefault();
        }

        public Status GetStatusByName(string name)
        {
            return _context.Status.Where(s => s.Name == name && s.IsExist == true).FirstOrDefault();
        }

        public bool StatusExist(int id)
        {
            return _context.Status.Any(s => s.Id == id && s.IsExist == true);
        }

        public bool AddStatus(Status status)
        {
            _context.Add(status);
            return Save();
        }

        public bool DeleteStatus(Status status)
        {
            status.IsExist = false;
            return Save();
        }

        public bool UpdateStatus(Status status)
        {
            _context.Update(status);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
