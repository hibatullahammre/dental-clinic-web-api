﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicWebApi.Repository
{
    public class UsersRepository : IUsersRepository
    {
        private readonly DataContext _context;

        public UsersRepository(DataContext context)
        {
            _context = context;
        }

        public bool AddUser(Users user)
        {
            _context.Add(user);
            return Save();
        }

        public bool DeleteUser(Users user)
        {
            user.IsExist = false;
            return Save();
        }

        public Users GetUserById(int userId)
        {
            return _context.Users.Include(u => u.Account).
                Where(u => u.IsExist.Value && u.Account.accountsType == Models.Enums.AccountType.User && u.Id == userId).FirstOrDefault();
        }

        public ICollection<Users> GetUsers()
        {
            return _context.Users.Include(u => u.Account)
                .Where(u => u.IsExist.Value && u.Account.accountsType == Models.Enums.AccountType.User).ToList();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }

        public bool UpdateUser(Users user)
        {
            _context.Update(user);
            return Save();
        }

        public bool UserExist(int userId)
        {
            return _context.Users.Any(u => u.Id == userId && u.IsExist.Value);
        }
    }
}
