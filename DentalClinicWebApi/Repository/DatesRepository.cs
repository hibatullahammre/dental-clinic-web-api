﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Repository
{
    public class DatesRepository : IDatesRepository
    {
        private DataContext _context;

        public DatesRepository(DataContext context)
        {
            _context = context;
        }

        public ICollection<Dates> GetDates()
        {
            return _context.Dates.Where(d => d.IsExist == true).ToList();
        }

        public Dates GetDate(int dateId)
        {
            return _context.Dates.Where(d => d.Id == dateId && d.IsExist == true).FirstOrDefault();
        }

        public ICollection<Dates> GetDates(DateTime Date)
        {
            return _context.Dates.Where(d => d.IsExist == true && d.Date.Date == Date.Date).ToList();
        }

        public ICollection<Dates> GetDatesByAccount(int accountId)
        {
            return _context.Dates.Where(d => d.IsExist == true && d.AccountId == accountId).ToList();
        }

        public bool DateExists(int dateId)
        {
            return _context.Dates.Any(d => d.Id == dateId && d.IsExist == true);
        }

        public bool CreateDate(Dates date)
        {
            _context.Add(date);
            return Save();
        }

        public bool DeleteDate(Dates date)
        {
            date.IsExist = false;
            return Save();
        }

        public bool UpdateDate(Dates date)
        {
            _context.Update(date);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
