﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace DentalClinicWebApi.Repository
{
    public class SessionsRepository : ISessionsRepository
    {
        private readonly DataContext _context;

        public SessionsRepository(DataContext context)
        {
            _context = context;
        }

        public ICollection<Sessions> GetDailySession(DateTime sessionDate)
        {
            return _context.Sessions.Where(s => s.IsExist.Value && s.Date.Date == sessionDate.Date).ToList();   
        }

        public ICollection<Sessions> GetPatientSessions(int accountId)
        {
            return _context.Sessions.Where(s => s.AccountID == accountId && s.IsExist.Value).ToList();
        }

        public Sessions GetSessionById(int sessionId)
        {
            return _context.Sessions.Where(s => s.Id == sessionId && s.IsExist.Value).FirstOrDefault();
        }

        public ICollection<Sessions> GetSessions()
        {
            return _context.Sessions.Where(s => s.IsExist == true).ToList();
        }

        public ICollection<Sessions> GetStatusSessionForpatient(int statusId, int accountId)
        {
            return _context.Sessions.Include(c => c.Status)
                .Where(s => s.AccountID == accountId && s.StatusID == statusId && s.IsExist.Value).ToList();
        }

        public bool SessionExist(int sessionId)
        {
            return _context.Sessions.Any(s => s.IsExist == true && s.Id == sessionId);
        }

        public bool UpdateSession(Sessions session)
        {
            _context.Update(session);
            return Save();
        }

        public bool AddSession(Sessions session)
        {
            _context.Add(session);
            return Save();
        }

        public bool DeleteSession(Sessions session)
        {
            session.IsExist = false;
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
