﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Repository
{
    public class AccountsRepository : IAccountsRepository
    {
        private DataContext _context;

        public AccountsRepository(DataContext context)
        {
            _context = context;
        }
        public ICollection<Accounts> GetAccounts()
        {
            return _context.Accounts.Where(a => a.IsExist == true).ToList();
        }

        public Accounts GetAccountByID(int accountId)
        {
            return _context.Accounts.Where(a => a.Id == accountId && a.IsExist == true).FirstOrDefault();
        }

        public Accounts GetAccountByName(string name)
        {
            return _context.Accounts.Where(a => a.Name == name && a.IsExist == true).FirstOrDefault();
        }

        public ICollection<Accounts> GetEmployees()
        {
            return _context.Accounts.Where(a =>a.IsExist == true && a.accountsType == Models.Enums.AccountType.Employee).ToList();
        }

        public ICollection<Accounts> GetPatients()
        {
            return _context.Accounts.Where(a => a.IsExist == true && a.accountsType == Models.Enums.AccountType.Patient).ToList();
        }

        public ICollection<Accounts> GetUsers()
        {
            return _context.Accounts.Where(a => a.IsExist == true && a.accountsType == Models.Enums.AccountType.User).ToList();
        }

        public bool IsAccountExist(int id)
        {
            return _context.Accounts.Any(a => a.Id == id && a.IsExist == true);
        }

        public bool CreateAccount(Accounts account)
        {
            _context.Add(account);
            return Save();
        }

        public bool UpdateAccount(Accounts account)
        {
            _context.Update(account);
            return Save();
        }

        public bool DeleteAccount(Accounts account)
        {
            //_context.Remove(accountId);
            account.IsExist = false;
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
