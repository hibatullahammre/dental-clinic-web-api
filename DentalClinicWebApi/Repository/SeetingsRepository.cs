﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Repository
{
    public class SeetingsRepository : ISeetingsRepository
    {
        private readonly DataContext _context;

        public SeetingsRepository(DataContext context)
        {
            _context = context;
        }

        public ICollection<Settings> GetSettings()
        {
            return _context.Settings.Where(s => s.IsExist.Value).ToList();
        }

        public Settings GetSettingsById(int settingId)
        {
            return _context.Settings.FirstOrDefault(s => s.Id == settingId && s.IsExist.Value);
        }

        public Settings GetSettingsByName(string name)
        {
            return _context.Settings.FirstOrDefault(s => s.IsExist.Value && s.Name == name);    
        }

        public bool SettingExist(int settingId)
        {
            return _context.Settings.Any(s => s.Id == settingId && s.IsExist.Value);
        }

        public bool UpdateSettings(Settings settings)
        {
            _context.Update(settings);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
