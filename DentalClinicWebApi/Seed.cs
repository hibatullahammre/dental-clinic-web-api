﻿using DentalClinicWebApi.Data;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi
{
    public class Seed
    {
        private readonly DataContext _context;
        public Seed(DataContext context)
        {
            _context = context;
        }

        public void SeedDataContext()
        {
            if (!_context.Settings.Any(s => s.IsExist.Value))
            {
                var seetings = new List<Settings>()
                {
                    new Settings()
                    {
                        Name = "DoctorName"
                    },
                    new Settings()
                    {
                        Name = "Phones"
                    },
                    new Settings()
                    {
                        Name = "Address"
                    }
                };
                _context.Settings.AddRange(seetings);
                _context.SaveChanges();
            }
        }
    }
}
