﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DentalClinicWebApi.Migrations
{
    public partial class edit_AccountsType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_AccountsTypes_accountsTypeId",
                table: "Accounts");

            migrationBuilder.DropTable(
                name: "AccountsTypes");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_accountsTypeId",
                table: "Accounts");

            migrationBuilder.RenameColumn(
                name: "accountsTypeId",
                table: "Accounts",
                newName: "accountsType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "accountsType",
                table: "Accounts",
                newName: "accountsTypeId");

            migrationBuilder.CreateTable(
                name: "AccountsTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsExist = table.Column<bool>(type: "bit", nullable: true),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountsTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_accountsTypeId",
                table: "Accounts",
                column: "accountsTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_AccountsTypes_accountsTypeId",
                table: "Accounts",
                column: "accountsTypeId",
                principalTable: "AccountsTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
