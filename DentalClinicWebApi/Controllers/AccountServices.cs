﻿using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Controllers
{
    public class AccountServices
    {
        private readonly IAccountsRepository _accountsRepository;
        private readonly IPatientsRepository _patientsRepository;
        private readonly IUsersRepository _usersRepository;

        public AccountServices(IAccountsRepository accountsRepository, IPatientsRepository patientsRepository, IUsersRepository usersRepository)
        {
            _accountsRepository = accountsRepository;
            _patientsRepository = patientsRepository;
            _usersRepository = usersRepository;
        }

        public void AddAccountAndPAtients(Accounts account, Patients patient)
        {
            _accountsRepository.CreateAccount(account);
            patient.AccountID = account.Id;
            _patientsRepository.AddPatient(patient);
        }

        public void UpdateAccountAndPatients(Accounts account, Patients patient)
        {
            _accountsRepository.UpdateAccount(account);
            patient.AccountID = account.Id;
            _patientsRepository.UpdatePatient(patient);
        }

        public void DeleteAccountAndPatients(Accounts account, Patients patient)
        {
            _accountsRepository.DeleteAccount(account);
            _patientsRepository.DeletePatient(patient);
        }

        public void AddAccountAndUsers(Accounts account, Users user)
        {
            _accountsRepository.CreateAccount(account);
            user.AccountsId = account.Id;
            _usersRepository.AddUser(user);
        }

        public void UpdateAccountAndUsers(Accounts account, Users user)
        {
            _accountsRepository.UpdateAccount(account);
            user.AccountsId = account.Id;
            _usersRepository.UpdateUser(user);
        }

        public void DeleteAccountAndUsers(Accounts account, Users user)
        {
            _accountsRepository.DeleteAccount(account);
            _usersRepository.DeleteUser(user);
        }
    }
}
