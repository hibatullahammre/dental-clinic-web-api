﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : Controller
    {
        private readonly IAccountsRepository _accountsRepository;
        private readonly IMapper _mapper;

        public AccountsController(IAccountsRepository accountsRepository, IMapper mapper)
        {
            _accountsRepository = accountsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType (200, Type = typeof(IEnumerable<Accounts>))]
        public IActionResult GetAccounts()
        {
            var accounts = _mapper.Map<List<AccountsDto>>(_accountsRepository.GetAccounts());
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(accounts);
        }

        //[HttpGet]
        //[ProducesResponseType(200, Type = typeof(IEnumerable<Accounts>))]
        //public IActionResult GetEmployees()
        //{
        //    var employees = _mapper.Map<AccountsDto>(_accountsRepository.GetEmployees());

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    return Ok(employees);
        //}

        //[HttpGet]
        //[ProducesResponseType(200, Type = typeof(IEnumerable<Accounts>))]
        //public IActionResult GetPatients()
        //{
        //    var patients = _mapper.Map<AccountsDto>(_accountsRepository.GetPatients());

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    return Ok(patients);
        //}

        //[HttpGet]
        //[ProducesResponseType(200, Type = typeof(IEnumerable<Accounts>))]
        //public IActionResult GetUsers()
        //{
        //    var users = _mapper.Map<AccountsDto>(_accountsRepository.GetUsers());

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    return Ok(users);
        //}

        [HttpGet("{accountId:int}")]
        [ProducesResponseType(200, Type = typeof(Accounts))]
        [ProducesResponseType(400)]
        public IActionResult GetAccountByID(int accountId)
        {
            if (!_accountsRepository.IsAccountExist(accountId))
                return NotFound();

            var account = _mapper.Map<AccountsDto>(_accountsRepository.GetAccountByID(accountId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(account);
        }

        [HttpGet("{name}")]
        [ProducesResponseType(200, Type=typeof(Accounts))]
        [ProducesResponseType(400)]
        public IActionResult GetAccountByName(string name)
        {
            var account = _mapper.Map<AccountsDto>(_accountsRepository.GetAccountByName(name));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(account);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult AddAccount([FromBody] AccountsDto account)
        {
            if (account == null)
                return BadRequest(ModelState);

            var accountExist = _accountsRepository.GetAccounts().
                Where(c => c.Name.Trim().ToUpper() == account.Name.TrimEnd().ToUpper() && c.IsExist == true).FirstOrDefault();

            if (accountExist != null)
            {
                ModelState.AddModelError("", "Account is Already Exist");
                return StatusCode(400, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var accountMap = _mapper.Map<Accounts>(account);

            if (!_accountsRepository.CreateAccount(accountMap))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{accountId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditAccount(int accountId, [FromBody] AccountsDto account)
        {
            if (account == null)
               return BadRequest(ModelState);

            if(account.Id != accountId)
                return BadRequest(ModelState);

            if(!_accountsRepository.IsAccountExist(accountId))
                return NotFound();

            var accountMap = _mapper.Map<Accounts>(account);

            if (!_accountsRepository.UpdateAccount(accountMap))
            {
                ModelState.AddModelError("", "Something went wrong while updating");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{accountId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteAccount(int accountId)
        {
            if(!_accountsRepository.IsAccountExist(accountId))
                return NotFound();

            var account = _accountsRepository.GetAccountByID(accountId);

            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            if(!_accountsRepository.DeleteAccount(account))
            {
                ModelState.AddModelError("", "Something went wrong while deleting");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
