﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController : Controller
    {
        private readonly IPatientsRepository _patientsRepository;
        private readonly IMapper _mapper;
        private readonly AccountServices _accountServices;

        public PatientsController(IPatientsRepository patientsRepository, IMapper mapper, AccountServices accountServices)
        {
            _patientsRepository = patientsRepository;
            _mapper = mapper;
            _accountServices = accountServices;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Patients>))]
        public IActionResult GetPatients()
        {
            var patients = _mapper.Map<List<PatientsDto>>(_patientsRepository.GetPatients());

            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(patients);
        }

        [HttpGet("{patientId}")]
        [ProducesResponseType(200, Type = typeof(Patients))]
        [ProducesResponseType(404)]
        public IActionResult GetPAtientsById(int patientId)
        {
            if (!_patientsRepository.PatientExist(patientId))
                return NotFound();

            var patient = _mapper.Map<PatientsDto>(_patientsRepository.GetPatientById(patientId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(patient);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult AddAccountAndPatients([FromBody] PatientsDto patient)
        {
            if (patient == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var patientMap = _mapper.Map<Patients>(patient); 
            var accountMap = _mapper.Map<Accounts>(patientMap.Account);

            _accountServices.AddAccountAndPAtients(accountMap, patientMap);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok("Successfully Created");
        }

        [HttpPut("{patientId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditPatient(int patientId, [FromBody] PatientsDto patient)
        {
            if (patient == null)
                return BadRequest(ModelState);

            if (patient.Id != patientId)
                return BadRequest(ModelState);

            if (!_patientsRepository.PatientExist(patientId))
                return NotFound();

            var patientMap = _mapper.Map<Patients>(patient);

            _accountServices.UpdateAccountAndPatients(patientMap.Account, patientMap);

            return NoContent();
        }

        [HttpDelete("{patientId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeletePatient(int patientId)
        {
            if (!_patientsRepository.PatientExist(patientId))
                return NotFound();

            var patient = _patientsRepository.GetPatientById(patientId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _accountServices.DeleteAccountAndPatients(patient.Account, patient);

            return NoContent();
        }
    }
}
