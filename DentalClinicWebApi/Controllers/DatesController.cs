﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatesController : Controller
    {
        private readonly IDatesRepository _datesRepository;
        private readonly IMapper _mapper;
        private readonly IAccountsRepository _accountsRepository;
        private readonly IStatusRepository _statusRepository;

        public DatesController(IDatesRepository datesRepository, IMapper mapper, IAccountsRepository accountsRepository, IStatusRepository statusRepository)
        {
            _datesRepository = datesRepository;
            _mapper = mapper;
            _accountsRepository = accountsRepository;
            _statusRepository = statusRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Dates>))]
        public IActionResult GetDates()
        {
            var dates = _mapper.Map<List<DatesDto>>(_datesRepository.GetDates());

            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(dates);
        }

        //[HttpGet("{dateId:int}")]
        //[ProducesResponseType(200, Type = typeof(Dates))]
        //public IActionResult GetDate(int dateId)
        //{
        //    var date = _mapper.Map<List<DatesDto>>(_datesRepository.GetDate(dateId));

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    return Ok(date);
        //}

        [HttpGet("{date:DateTime}")]
        [ProducesResponseType(200, Type =typeof(IEnumerable<Dates>))]   
        public IActionResult GetDates(DateTime date)
        {
            var dates = _mapper.Map<List<DatesDto>>(_datesRepository.GetDates(date));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(dates);
        }

        [HttpGet("{accountId:int}")]
        [ProducesResponseType(200, Type =typeof(IEnumerable<Dates>))]
        public IActionResult GetDatesByAccount(int accountId)
        {
            var dates = _mapper.Map<List<DatesDto>>(_datesRepository.GetDatesByAccount(accountId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(dates);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult AddDate([FromBody] DatesDto date, [FromQuery] int accountId, [FromQuery] int statusId)
        {
            if (date == null)
                BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var dateMap = _mapper.Map<Dates>(date);

            dateMap.Account = _accountsRepository.GetAccountByID(accountId);
            dateMap.Status = _statusRepository.GetStatusById(statusId);

            if (!_datesRepository.CreateDate(dateMap))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{dateId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult EditDate(int dateId, [FromBody] DatesDto date, [FromQuery] int accountId, [FromQuery] int statusId)
        {
            if(date == null)
                return BadRequest(ModelState);

            if (date.Id != dateId)
                return BadRequest(ModelState);

            if (!_datesRepository.DateExists(dateId))
                return NotFound();

            var dateMap = _mapper.Map<Dates>(date);

            dateMap.Account = _accountsRepository.GetAccountByID(accountId);
            dateMap.Status = _statusRepository.GetStatusById(statusId);

            if (!_datesRepository.UpdateDate(dateMap))
            {
                ModelState.AddModelError("", "Something went wrong while Editing");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{dateId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult DeleteDate(int dateId)
        {
            if (!_datesRepository.DateExists(dateId))
                return NotFound();

            var date = _datesRepository.GetDate(dateId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_datesRepository.DeleteDate(date))
            {
                ModelState.AddModelError("", "Something went wrong while deleting");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
