﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly AccountServices _accountServices;
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public UsersController(AccountServices accountServices, IUsersRepository usersRepository, IMapper mapper)
        {
            _accountServices = accountServices;
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Users>))]
        public IActionResult GetUsers()
        {
            var users = _mapper.Map<List<UsersDto>>(_usersRepository.GetUsers());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(users);
        }

        [HttpGet("{userId}")]
        [ProducesResponseType(200, Type = typeof(Users))]
        [ProducesResponseType(404)]
        public IActionResult GetUsersById(int userId)
        {
            if (!_usersRepository.UserExist(userId))
                return NotFound();

            var user = _mapper.Map<UsersDto>(_usersRepository.GetUserById(userId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(user);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult AddAccountAndUsers([FromBody] UsersDto user)
        {
            if (user == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userMap = _mapper.Map<Users>(user);
            var accountMap = _mapper.Map<Accounts>(userMap.Account);

            _accountServices.AddAccountAndUsers(accountMap, userMap);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok("Successfully Created");
        }

        [HttpPut("{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditUser(int userId, [FromBody] UsersDto user)
        {
            if (user == null)
                return BadRequest(ModelState);

            if (user.Id != userId)
                return BadRequest(ModelState);

            if (!_usersRepository.UserExist(userId))
                return NotFound();

            var userMap = _mapper.Map<Users>(user);

            _accountServices.UpdateAccountAndUsers(userMap.Account, userMap);

            return NoContent();
        }

        [HttpDelete("{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteUser(int userId)
        {
            if (!_usersRepository.UserExist(userId))
                return NotFound();

            var user = _usersRepository.GetUserById(userId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _accountServices.DeleteAccountAndUsers(user.Account, user);

            return NoContent();
        }
    }
}
