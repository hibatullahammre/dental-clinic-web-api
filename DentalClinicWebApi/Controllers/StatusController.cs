﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : Controller
    {
        private readonly IStatusRepository _statusRepository;
        private readonly IMapper _mapper;

        public StatusController(IStatusRepository statusRepository, IMapper mapper)
        {
            _statusRepository = statusRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Status>))]
        public IActionResult GetStatus()
        {
            var status = _mapper.Map<List<StatusDto>>(_statusRepository.GetStatus());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(status);
        }

        [HttpGet("{statusId:int}")]
        [ProducesResponseType(200 , Type = typeof(Status))]
        public IActionResult GetStatusById(int statusId)
        {
            var status = _mapper.Map<StatusDto>(_statusRepository.GetStatusById(statusId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(status);
        }

        [HttpGet("{name}")]
        [ProducesResponseType(200, Type = typeof(Status))]
        public IActionResult GetStatusByName(string name)
        {
            var status = _mapper.Map<StatusDto>(_statusRepository.GetStatusByName(name));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(status);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult AddStatus([FromBody] StatusDto Status)
        {
            if (Status == null)
                return BadRequest(ModelState);

            var statusExist = _statusRepository.GetStatus().
                Where(s => s.Name.Trim().ToUpper() == Status.Name.TrimEnd().ToUpper() && s.IsExist == true).FirstOrDefault();

            if (statusExist != null)
            {
                ModelState.AddModelError("", "Status is Already Exist");
                return StatusCode(400, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var statusMap = _mapper.Map<Status>(Status);

            if (!_statusRepository.AddStatus(statusMap))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{statusId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditAccount(int statusId, [FromBody] StatusDto status)
        {
            if (status == null)
                return BadRequest(ModelState);

            if (status.Id != statusId)
                return BadRequest(ModelState);

            if (!_statusRepository.StatusExist(statusId))
                return NotFound();

            var statusMap = _mapper.Map<Status>(status);

            if (!_statusRepository.UpdateStatus(statusMap))
            {
                ModelState.AddModelError("", "Something went wrong while updating");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{statusId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteStatus(int statusId)
        {
            if (!_statusRepository.StatusExist(statusId))
                return NotFound();

            var status = _statusRepository.GetStatusById(statusId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_statusRepository.DeleteStatus(status))
            {
                ModelState.AddModelError("", "Something went wrong while deleting");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
