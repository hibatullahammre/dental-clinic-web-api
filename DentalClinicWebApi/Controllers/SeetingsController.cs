﻿using Microsoft.AspNetCore.Mvc;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeetingsController : Controller
    {
        private readonly ISeetingsRepository _seetingsRepository;

        public SeetingsController(ISeetingsRepository seetingsRepository)
        {
            _seetingsRepository = seetingsRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Settings>))]
        public IActionResult GetSettings()
        {
            var seetings = _seetingsRepository.GetSettings();

            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(seetings);
        }

        [HttpGet("{settingId:int}")]
        [ProducesResponseType(200, Type = typeof(Settings))]
        public IActionResult GetSettingsById(int settingId)
        {
            var setting = _seetingsRepository.GetSettingsById(settingId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(setting);
        }

        [HttpGet("{name}")]
        [ProducesResponseType(200, Type = typeof(Settings))]
        public IActionResult GetSettingsByName(string name)
        {
            var setting = _seetingsRepository.GetSettingsByName(name);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(setting);
        }

        [HttpPut("{settingId},{name}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult UpdateSettings(string name, int settingId, [FromBody] Settings setting)
        {
            if (setting == null)
                return BadRequest(ModelState);

            if(setting.Name != name)
                return BadRequest(ModelState);

            if(!_seetingsRepository.SettingExist(settingId))
                return NotFound();

            if(!_seetingsRepository.UpdateSettings(setting))
            {
                ModelState.AddModelError("", "Something wrong while Saving");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
