﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : Controller
    {
        private readonly ISessionsRepository _sessionsRepository;
        private readonly IMapper _mapper;

        public SessionsController(ISessionsRepository sessionsRepository, IMapper mapper)
        {
            _sessionsRepository = sessionsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Sessions>))]
        public IActionResult GetSessions()
        {
            var sessions = _mapper.Map<List<SessionsDto>>(_sessionsRepository.GetSessions());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(sessions);
        }

        [HttpGet("{sessionId:int}")]
        [ProducesResponseType(200, Type = typeof (Sessions))]
        [ProducesResponseType(400)]
        public IActionResult GetSessionsById(int sessionId)
        {
            var session = _mapper.Map<SessionsDto>(_sessionsRepository.GetSessionById(sessionId));
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(session);
        }

        [HttpGet("{accountId:int}/Sessions")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Sessions>))]
        public IActionResult GetSessionsByAccount(int accountId)
        {
            var sessions = _mapper.Map<List<SessionsDto>>(_sessionsRepository.GetPatientSessions(accountId));
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(sessions);
        }

        [HttpGet("{sessionDate}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Sessions>))]
        public IActionResult GetDailySessions(DateTime sessionDate)
        {
            var sessions = _mapper.Map<List<SessionsDto>>(_sessionsRepository.GetDailySession(sessionDate));
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(sessions);
        }

        [HttpGet("{accountId},{statusId}/Session")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Sessions>))]
        public IActionResult GetStatusSessionForpatient(int accountId, int statusId)
        {
            var sessions = _mapper.Map<List<SessionsDto>>(_sessionsRepository.GetStatusSessionForpatient(statusId, accountId));
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(sessions);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult AddSession([FromBody] SessionsDto session)
        {
            if (session == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var sessionMap = _mapper.Map<Sessions>(session);

            if (!_sessionsRepository.AddSession(sessionMap))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{sessionId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult UpdateSession([FromBody] SessionsDto session, int sessionId)
        {
            if (session == null)
                return BadRequest(ModelState);

            if(session.Id != sessionId)
                return BadRequest(ModelState);

            if(!_sessionsRepository.SessionExist(sessionId))
                return NotFound();

            var sessionMap = _mapper.Map<Sessions>(session);

            if (!_sessionsRepository.UpdateSession(sessionMap))
            {
                ModelState.AddModelError("", "Something went wrong while updating");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{sessionId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteSession(int sessionId)
        {
            if (!_sessionsRepository.SessionExist(sessionId))
                return NotFound();

            var session = _sessionsRepository.GetSessionById(sessionId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_sessionsRepository.DeleteSession(session))
            {
                ModelState.AddModelError("", "Something went wrong while deleting");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
