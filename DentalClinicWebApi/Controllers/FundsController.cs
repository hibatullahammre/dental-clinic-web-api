﻿using AutoMapper;
using DentalClinicWebApi.Dto;
using DentalClinicWebApi.Interfaces;
using DentalClinicWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinicWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FundsController : Controller 
    {
        private readonly IFundsRepository _fundsRepository;
        private readonly IMapper _mapper;

        public FundsController(IFundsRepository fundsRepository, IMapper mapper)
        {
            _fundsRepository = fundsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Funds>))]
        public IActionResult GetFunds()
        {
            var funds = _mapper.Map<List<FundsDto>>(_fundsRepository.GetFunds());

            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(funds);
        }

        [HttpGet("{fundId:int}")]
        [ProducesResponseType(200, Type = typeof(Funds))]
        [ProducesResponseType(404)]
        public IActionResult GetFundById(int fundId)
        {
            if (!_fundsRepository.FundsExist(fundId))
                return NotFound();

            var fund = _mapper.Map<FundsDto>(_fundsRepository.GetFundsById(fundId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(fund);
        }

        [HttpGet("{start:DateTime},{end:DateTime}/Funds")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Funds>))]
        public IActionResult GetMonthlyFunds(DateTime start, DateTime end)
        {
            var funds = _mapper.Map<List<FundsDto>>(_fundsRepository.GetMonthlyFunds(start, end));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(funds);
        }

        [HttpGet("{accountId:int},{date:DateTime}/Funds")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Funds>))]
        public IActionResult GetAccountCheckUp(int accountId, DateTime date)
        {
            var funds = _mapper.Map<List<FundsDto>>(_fundsRepository.GetAccountCheckUp(accountId, date));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(funds);
        }

        [HttpGet("{date}/Funds")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Funds>))]
        public IActionResult GetDailyFunds(DateTime date)
        {
            var funds = _mapper.Map<List<FundsDto>>(_fundsRepository.GetDailyFunds(date));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(funds);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult AddFunds([FromBody] FundsDto fund)
        {
            if (fund == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var fundMap = _mapper.Map<Funds>(fund);

            if (!_fundsRepository.AddFunds(fundMap))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{fundId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult EditFund(int fundId, [FromBody] FundsDto fund)
        {
            if (fund == null)
                return BadRequest(ModelState);

            if (fund.Id != fundId)
                return BadRequest(ModelState);

            if (!_fundsRepository.FundsExist(fundId))
                return NotFound();

            var fundMap = _mapper.Map<Funds>(fund);

            if (!_fundsRepository.UpdateFunds(fundMap))
            {
                ModelState.AddModelError("", "Something went wrong while updating");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{fundId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult DeleteFunds(int fundId)
        {
            if (!_fundsRepository.FundsExist(fundId))
                return NotFound();

            var fund = _fundsRepository.GetFundsById(fundId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_fundsRepository.DeleteFunds(fund))
            {
                ModelState.AddModelError("", "Something went wrong while deleting");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }
    }
}
